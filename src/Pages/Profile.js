import React from "react";
import {
  Header,
  Segment,
  Form
} from "semantic-ui-react";

export default () => (
  <Segment basic>
    <Header as="h1">Profile</Header>
    <Form>
      <Form.Input
        fluid
        label="Vorname"
        id="first-name" />
      <Form.Input
        fluid
        label="Nachname"
        id="last-name" />
    </Form>
  </Segment>
);

