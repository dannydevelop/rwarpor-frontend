import React from "react";
import {
  Header,
  Segment
} from "semantic-ui-react";

export default () => (
  <Segment basic>
    <Header as="h1">Kontakt</Header>
    <p>kontakt@rwa.at</p>
  </Segment>
);