// OrderNumber, Date, Client, BusinessArea, ClientOrderNumber, Supplier, 
// ArticleNumber, ... 
const MockOrders = [
    {
        OrderNumber: "9127362",
        Date: "18.12.2019",
        Client: "7600",
        BusinessArea: "Baustoffe",
        ClientOrderNumber: "1154034",
        Supplier: "KLK",
        OrderEntries: [
            {
                RWAArticleNumber: "30315704",
                ArticleName: "STANDVENTILATOR COOLBR_4000_SV D40CM",
                AmountUnit: "2 Stk.",
                UnitPrice: "95,192€",
                PositionPrice: "1903,84€",
                Status: "erledigt"
            }, {
                RWAArticleNumber: "30315704",
                ArticleName: "STANDVENTILATOR COOLBR_4000_SV D40CM",
                AmountUnit: "2 Stk.",
                UnitPrice: "95,192€",
                PositionPrice: "1903,84€",
                Status: "erledigt"
            }, {
                RWAArticleNumber: "30315704",
                ArticleName: "STANDVENTILATOR COOLBR_4000_SV D40CM",
                AmountUnit: "2 Stk.",
                UnitPrice: "95,192€",
                PositionPrice: "1903,84€",
                Status: "erledigt"
            },
        ]
    },
    {
        OrderNumber: "9127361",
        Date: "18.12.2019",
        Client: "7600",
        BusinessArea: "Baustoffe",
        ClientOrderNumber: "1154034",
        Supplier: "KLK",
        OrderEntries: [
            {
                RWAArticleNumber: "30315704",
                ArticleName: "STANDVENTILATOR COOLBR_4000_SV D40CM",
                AmountUnit: "2 Stk.",
                UnitPrice: "95,192€",
                PositionPrice: "1903,84€",
                Status: "erledigt"
            }, {
                RWAArticleNumber: "30315704",
                ArticleName: "STANDVENTILATOR COOLBR_4000_SV D40CM",
                AmountUnit: "2 Stk.",
                UnitPrice: "95,192€",
                PositionPrice: "1903,84€",
                Status: "erledigt"
            }, {
                RWAArticleNumber: "30315704",
                ArticleName: "STANDVENTILATOR COOLBR_4000_SV D40CM",
                AmountUnit: "2 Stk.",
                UnitPrice: "95,192€",
                PositionPrice: "1903,84€",
                Status: "erledigt"
            },
        ]
    },
    {
        OrderNumber: "9127360",
        Date: "18.12.2019",
        Client: "7600",
        BusinessArea: "Baustoffe",
        ClientOrderNumber: "1154034",
        Supplier: "KLK",
        OrderEntries: [
            {
                RWAArticleNumber: "30315704",
                ArticleName: "STANDVENTILATOR COOLBR_4000_SV D40CM",
                AmountUnit: "2 Stk.",
                UnitPrice: "95,192€",
                PositionPrice: "1903,84€",
                Status: "erledigt"
            }, {
                RWAArticleNumber: "30315704",
                ArticleName: "STANDVENTILATOR COOLBR_4000_SV D40CM",
                AmountUnit: "2 Stk.",
                UnitPrice: "95,192€",
                PositionPrice: "1903,84€",
                Status: "erledigt"
            }, {
                RWAArticleNumber: "30315704",
                ArticleName: "DACHSCHAUFEL_HEIZLASTER_WAGEN 3000 C",
                AmountUnit: "2 Stk.",
                UnitPrice: "934,192€",
                PositionPrice: "13,84€",
                Status: "erledigt"
            },
            {
                RWAArticleNumber: "30315704",
                ArticleName: "STANDVENTILATOR COOLBR_4000_SV D40CM",
                AmountUnit: "2 Stk.",
                UnitPrice: "934,192€",
                PositionPrice: "13,84€",
                Status: "erledigt"
            },
            {
                RWAArticleNumber: "30315704",
                ArticleName: "STANDVENTILATOR COOLBR_4000_SV D40CM",
                AmountUnit: "2 Stk.",
                UnitPrice: "934,192€",
                PositionPrice: "13,84€",
                Status: "erledigt"
            },
            {
                RWAArticleNumber: "30315704",
                ArticleName: "STANDVENTILATOR COOLBR_4000_SV D40CM",
                AmountUnit: "2 Stk.",
                UnitPrice: "934,192€",
                PositionPrice: "13,84€",
                Status: "erledigt"
            },
            {
                RWAArticleNumber: "30315704",
                ArticleName: "STANDVENTILATOR COOLBR_4000_SV D40CM",
                AmountUnit: "2 Stk.",
                UnitPrice: "934,192€",
                PositionPrice: "13,84€",
                Status: "erledigt"
            },
            {
                RWAArticleNumber: "30315704",
                ArticleName: "DACHSCHAUFEL_HEIZLASTER_WAGEN 3000 C",
                AmountUnit: "2 Stk.",
                UnitPrice: "934,192€",
                PositionPrice: "13,84€",
                Status: "erledigt"
            },
            {
                RWAArticleNumber: "30315704",
                ArticleName: "DACHSCHAUFEL_HEIZLASTER_WAGEN 3000 C",
                AmountUnit: "2 Stk.",
                UnitPrice: "934,192€",
                PositionPrice: "13,84€",
                Status: "erledigt"
            },
            {
                RWAArticleNumber: "30315704",
                ArticleName: "DACHSCHAUFEL_HEIZLASTER_WAGEN 3000 C",
                AmountUnit: "2 Stk.",
                UnitPrice: "934,192€",
                PositionPrice: "13,84€",
                Status: "erledigt"
            },
            {
                RWAArticleNumber: "30315704",
                ArticleName: "DACHSCHAUFEL_HEIZLASTER_WAGEN 3000 C",
                AmountUnit: "2 Stk.",
                UnitPrice: "934,192€",
                PositionPrice: "13,84€",
                Status: "erledigt"
            },
            {
                RWAArticleNumber: "30315704",
                ArticleName: "DACHSCHAUFEL_HEIZLASTER_WAGEN 3000 C",
                AmountUnit: "2 Stk.",
                UnitPrice: "934,192€",
                PositionPrice: "13,84€",
                Status: "erledigt"
            },
            {
                RWAArticleNumber: "30315704",
                ArticleName: "DACHSCHAUFEL_HEIZLASTER_WAGEN 3000 C",
                AmountUnit: "2 Stk.",
                UnitPrice: "934,192€",
                PositionPrice: "13,84€",
                Status: "erledigt"
            },
            {
                RWAArticleNumber: "30315704",
                ArticleName: "DACHSCHAUFEL_HEIZLASTER_WAGEN 3000 C",
                AmountUnit: "2 Stk.",
                UnitPrice: "934,192€",
                PositionPrice: "13,84€",
                Status: "erledigt"
            },
            {
                RWAArticleNumber: "30315704",
                ArticleName: "DACHSCHAUFEL_HEIZLASTER_WAGEN 3000 C",
                AmountUnit: "2 Stk.",
                UnitPrice: "934,192€",
                PositionPrice: "13,84€",
                Status: "erledigt"
            },
        ]
    },
    {
        OrderNumber: "9127358",
        Date: "18.12.2019",
        Client: "7600",
        BusinessArea: "Baustoffe",
        ClientOrderNumber: "1154034",
        Supplier: "KLK",
        OrderEntries: [
            {
                RWAArticleNumber: "30315704",
                ArticleName: "STANDVENTILATOR COOLBR_4000_SV D40CM",
                AmountUnit: "2 Stk.",
                UnitPrice: "95,192€",
                PositionPrice: "1903,84€",
                Status: "erledigt"
            }, {
                RWAArticleNumber: "30315704",
                ArticleName: "STANDVENTILATOR COOLBR_4000_SV D40CM",
                AmountUnit: "2 Stk.",
                UnitPrice: "95,192€",
                PositionPrice: "1903,84€",
                Status: "erledigt"
            }, {
                RWAArticleNumber: "30315704",
                ArticleName: "STANDVENTILATOR COOLBR_4000_SV D40CM",
                AmountUnit: "2 Stk.",
                UnitPrice: "95,192€",
                PositionPrice: "1903,84€",
                Status: "erledigt"
            },
        ]
    },
    {
        OrderNumber: "9127339",
        Date: "18.12.2019",
        Client: "7600",
        BusinessArea: "Baustoffe",
        ClientOrderNumber: "1154034",
        Supplier: "KLK",
        OrderEntries: [
            {
                RWAArticleNumber: "30315704",
                ArticleName: "STANDVENTILATOR COOLBR_4000_SV D40CM",
                AmountUnit: "2 Stk.",
                UnitPrice: "95,192€",
                PositionPrice: "1903,84€",
                Status: "erledigt"
            }, {
                RWAArticleNumber: "30315704",
                ArticleName: "STANDVENTILATOR COOLBR_4000_SV D40CM",
                AmountUnit: "2 Stk.",
                UnitPrice: "95,192€",
                PositionPrice: "1903,84€",
                Status: "erledigt"
            }, {
                RWAArticleNumber: "30315704",
                ArticleName: "STANDVENTILATOR COOLBR_4000_SV D40CM",
                AmountUnit: "2 Stk.",
                UnitPrice: "95,192€",
                PositionPrice: "1903,84€",
                Status: "erledigt"
            },
        ]
    },
];

export default MockOrders;