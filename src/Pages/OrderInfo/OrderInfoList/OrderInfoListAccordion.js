
import React, { useState } from "react";
import { Input, Grid, Header, Button, Icon, Accordion, GridRow, GridColumn, Segment, Checkbox } from "semantic-ui-react";
import "./OrderInfoListAccordion.css"
import MockOrders from "./MockOrders.js";


const ArticleHeader = () => (
    <div className="OrderInfoListAccordion__Article__Header">
        <div className="OrderInfoListAccordion__Article__Header__Cell">RWA-Art. Nummer <Icon name="caret down"/></div>
        <div className="OrderInfoListAccordion__Article__Header__Cell">Artikelbezeichnung <Icon name="caret down"/></div>
        <div className="OrderInfoListAccordion__Article__Header__Cell">Menge/ Einheit <Icon name="caret down"/></div>
        <div className="OrderInfoListAccordion__Article__Header__Cell">Preis <Icon name="caret down"/></div>
        <div className="OrderInfoListAccordion__Article__Header__Cell">PosWert <Icon name="caret down"/></div>
        <div className="OrderInfoListAccordion__Article__Header__Cell">Status <Icon name="caret down"/></div>
        <div className="OrderInfoListAccordion__Article__Header__Cell">Retoure <Icon name="caret down"/></div>
    </div>
);

const OrderInfoListAccordion = (props) => {

    const [isFiltered, setIsFiltered] = useState(false);

    return (
        <div className="OrderInfoList">
            {!isFiltered &&
                <div className="OrderInfoListAccordion__QuickLinks">
                    <Grid columns="13" padded="vertically">
                        <GridRow stretched>
                            <GridColumn width="3">
                                <Segment basic className="OrderInfoListAccordion__QuickLinks__Guide">
                                    <Header as="h3">So funktioniert's</Header>
                                    <p>Mit den gewünschten Eckdaten finden Sie rasch Ihren Auftrag um:</p>
                                    <p>- den <b>Status</b> abzufragen<br />
                                        - eine <b>Retoure</b> anzusto&szlig;en<br />
                                        - etc.</p>
                                </Segment>
                            </GridColumn>
                            <GridColumn width="13">
                                <Segment basic className="OrderInfoListAccordion__QuickLinks__Options">
                                    <Header as="h3">Häufig gesucht</Header>
                                    <Grid columns="4">
                                        <GridRow>
                                            <GridColumn>
                                                <Button fluid basic primary onClick={() => setIsFiltered(true)}>Heute</Button>
                                            </GridColumn>
                                            <GridColumn>
                                                <Button fluid basic primary onClick={() => setIsFiltered(true)}>Diese Woche</Button>
                                            </GridColumn>
                                            <GridColumn>
                                                <Button fluid basic primary onClick={() => setIsFiltered(true)}>Diesen Monat</Button>
                                            </GridColumn>
                                            <GridColumn>
                                                <Button fluid basic primary onClick={() => setIsFiltered(true)}>offene Aufträge</Button>
                                            </GridColumn>
                                        </GridRow>
                                        <GridRow>
                                            <GridColumn>
                                                <Button fluid basic primary onClick={() => setIsFiltered(true)}>... etc.</Button>
                                            </GridColumn>
                                            <GridColumn>
                                                <Button fluid basic primary onClick={() => setIsFiltered(true)}>Die letzten 30</Button>
                                            </GridColumn>
                                        </GridRow>
                                    </Grid>
                                </Segment>
                            </GridColumn>
                        </GridRow>
                    </Grid>
                </div>
            }
            {isFiltered &&
                <div>
                    <div className="OrderInfoListAccordion__Header">
                        <div className="OrderInfoListAccordion__Header__Item OrderInfoListAccordion__Header__Item--Status">4 Aufträge gefunden</div>
                        <div className="OrderInfoListAccordion__Header__Item OrderInfoListAccordion__Header__Item--Search">
                            <Input icon="search" placeholder="Suchen ..." />
                        </div>
                    </div>

                    <div className="OrderInfoListAccordion__Results">
                        <div className="OrderInfoListAccordion__Results__Header">
                            <div className="OrderInfoListAccordion__Results__Header__Cell">RWA Auftr.Nr. <Icon name="caret down"/></div>
                            <div className="OrderInfoListAccordion__Results__Header__Cell">Datum <Icon name="caret down"/></div>
                            <div className="OrderInfoListAccordion__Results__Header__Cell">Auftraggeber <Icon name="caret down"/></div>
                            <div className="OrderInfoListAccordion__Results__Header__Cell">Sparte <Icon name="caret down"/></div>
                            <div className="OrderInfoListAccordion__Results__Header__Cell">Kunden Best.Nr. <Icon name="caret down"/></div>
                            <div className="OrderInfoListAccordion__Results__Header__Cell">Lieferant <Icon name="caret down"/></div>
                        </div>

                        <Accordion
                            fluid
                            panels={MockOrders.map(entry => {
                                return {
                                    key: entry.OrderNumber,
                                    class: "div",
                                    title: {
                                        className: "OrderInfoListAccordion__Results__Item",
                                        children: [
                                            <div key={entry.OrderNumber + '__OrderNumber'} className="OrderInfoListAccordion__Results__Item__Cell"><Icon name="caret right"/> {entry.OrderNumber}</div>,
                                            <div key={entry.OrderNumber + '__Date'} className="OrderInfoListAccordion__Results__Item__Cell">{entry.Date}</div>,
                                            <div key={entry.OrderNumber + '__Client'} className="OrderInfoListAccordion__Results__Item__Cell">{entry.Client}</div>,
                                            <div key={entry.OrderNumber + '__BusinessArea'} className="OrderInfoListAccordion__Results__Item__Cell">{entry.BusinessArea}</div>,
                                            <div key={entry.OrderNumber + '__OrderNumber'} className="OrderInfoListAccordion__Results__Item__Cell">{entry.OrderNumber}</div>,
                                            <div key={entry.OrderNumber + '__Supplier'} className="OrderInfoListAccordion__Results__Item__Cell">{entry.Supplier}</div>
                                        ],
                                    },
                                    content: { children: <OrderEntries entries={entry.OrderEntries} /> }
                                }
                            })}>
                        </Accordion>
                    </div>
                </div>
            }
        </div>

    );
};

const Orders = () => {
    return []
}

const OrderEntries = (props) => {

    return (
        <div className="OrderInfoListAccordion__Articles">
            <ArticleHeader/>
            <div className="OrderInfoListAccordion__Article__Items">
                {props.entries.map(entry => (
                    <div className="OrderInfoListAccordion__Article__Item">
                        <div className="OrderInfoListAccordion__Article__Item__Cell">{entry.RWAArticleNumber}</div>
                        <div className="OrderInfoListAccordion__Article__Item__Cell">{entry.ArticleName}</div>
                        <div className="OrderInfoListAccordion__Article__Item__Cell">{entry.AmountUnit}</div>
                        <div className="OrderInfoListAccordion__Article__Item__Cell">{entry.UnitPrice}</div>
                        <div className="OrderInfoListAccordion__Article__Item__Cell">{entry.PositionPrice}</div>
                        <div className="OrderInfoListAccordion__Article__Item__Cell">{entry.Status}</div>
                        <div className="OrderInfoListAccordion__Article__Item__Cell"><Checkbox/></div>
                    </div>
                ))}
            </div>
        </div>
    );

}

export default OrderInfoListAccordion;