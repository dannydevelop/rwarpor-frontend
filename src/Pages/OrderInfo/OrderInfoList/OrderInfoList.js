import React, { useState } from "react";
import { Input, Table, Checkbox, Grid, Header, Button, GridRow, GridColumn, Segment } from "semantic-ui-react";
import "./OrderInfoList.css"

const OrderInfoList = (props) => {

    const [isFiltered, setIsFiltered] = useState(false);

    return (
        <div className="OrderInfoList">
            {!isFiltered &&
                <div className="OrderInfoList__QuickLinks">
                    <Grid columns="13" padded="vertically">
                        <GridRow stretched>
                            <GridColumn width="3">
                                <Segment basic className="OrderInfoList__QuickLinks__Guide">
                                    <Header as="h3">So funktioniert's</Header>
                                    <p>Mit den gewünschten Eckdaten finden Sie rasch Ihren Auftrag um:</p>
                                    <p>- den <b>Status</b> abzufragen<br />
                                        - eine <b>Retoure</b> anzusto&szlig;en<br />
                                        - etc.</p>
                                </Segment>
                            </GridColumn>
                            <GridColumn width="13">
                                <Segment basic className="OrderInfoList__QuickLinks__Options">
                                    <Header as="h3">Häufig gesucht</Header>
                                    <Grid columns="4">
                                        <GridRow>
                                            <GridColumn>
                                                <Button fluid onClick={() => setIsFiltered(true)}>Heute</Button>
                                            </GridColumn>
                                            <GridColumn>
                                                <Button fluid onClick={() => setIsFiltered(true)}>Diese Woche</Button>
                                            </GridColumn>
                                            <GridColumn>
                                                <Button fluid onClick={() => setIsFiltered(true)}>Diesen Monat</Button>
                                            </GridColumn>
                                            <GridColumn>
                                                <Button fluid onClick={() => setIsFiltered(true)}>offene Aufträge</Button>
                                            </GridColumn>
                                        </GridRow>
                                        <GridRow>
                                            <GridColumn>
                                                <Button fluid onClick={() => setIsFiltered(true)}>... etc.</Button>
                                            </GridColumn>
                                            <GridColumn>
                                                <Button fluid onClick={() => setIsFiltered(true)}>Die letzten 30</Button>
                                            </GridColumn>
                                        </GridRow>
                                    </Grid>
                                </Segment>
                            </GridColumn>
                        </GridRow>
                    </Grid>
                </div>
            }
            {isFiltered &&
                <div>
                    <div className="OrderInfoList__Header">
                        <div className="OrderInfoList__Header__Item OrderInfoList__Header__Item--Status">4 Aufträge gefunden</div>
                        <div className="OrderInfoList__Header__Item OrderInfoList__Header__Item--Search">
                            <Input icon="search" placeholder="Suchen ..." />
                        </div>
                    </div>

                    <Table className="OrderInfoList__Results" selectable>
                        <Table.Header className="OrderInfoList__Results__Header">
                            <Table.Row verticalAlign="bottom">
                                <Table.HeaderCell>RWA Auftr.Nr.</Table.HeaderCell>
                                <Table.HeaderCell>Datum</Table.HeaderCell>
                                <Table.HeaderCell>Auftrag- geber</Table.HeaderCell>
                                <Table.HeaderCell>Sparte</Table.HeaderCell>
                                <Table.HeaderCell>Kunden Best.Nr.</Table.HeaderCell>
                                <Table.HeaderCell>Lieferant</Table.HeaderCell>
                                <Table.HeaderCell>RWA-Art. Nummer</Table.HeaderCell>
                                <Table.HeaderCell>Artikelbezeichnung</Table.HeaderCell>
                                <Table.HeaderCell>Menge/ Einheit</Table.HeaderCell>
                                <Table.HeaderCell>Preis</Table.HeaderCell>
                                <Table.HeaderCell>PosWert</Table.HeaderCell>
                                <Table.HeaderCell>Status</Table.HeaderCell>
                                <Table.HeaderCell>Retoure</Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>

                        <Table.Body>
                            <Table.Row className="OrderInfoList__Position OrderInfoList__Position--Order">
                                <Table.HeaderCell className="OrderInfoList__Cell--OrderNumber">9127362</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Date">18.12.2019</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Client">7600</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--BusinessArea">Baustoffe</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--ClientOrderNumber">1154034</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Supplier">KLK</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--ArticleNumber">30315704</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Name">STANDVENTILATOR COOLBR_4000_SV D40CM</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Quantity">2 Stk.</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--UnitPrice">951,92&euro;</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Price">1903,84 &euro;</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Status">erledigt</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Return"><Checkbox /></Table.HeaderCell>
                            </Table.Row>
                            <Table.Row className="OrderInfoList__Position">
                                <Table.HeaderCell colspan="6"></Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--ArticleNumber">30315704</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Name">STANDVENTILATOR COOLBR_4000_SV D40CM</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Quantity">2 Stk.</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--UnitPrice">951,92&euro;</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Price">1903,84 &euro;</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Status">erledigt</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Return"><Checkbox /></Table.HeaderCell>
                            </Table.Row>
                            <Table.Row className="OrderInfoList__Position">
                                <Table.HeaderCell colspan="6"></Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--ArticleNumber">30315704</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Name">STANDVENTILATOR COOLBR_4000_SV D40CM</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Quantity">2 Stk.</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--UnitPrice">951,92&euro;</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Price">1903,84 &euro;</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Status">erledigt</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Return"><Checkbox /></Table.HeaderCell>
                            </Table.Row>

                            <Table.Row className="OrderInfoList__Position OrderInfoList__Position--Order">
                                <Table.HeaderCell className="OrderInfoList__Cell--OrderNumber">9127362</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Date">18.12.2019</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Client">7600</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--BusinessArea">Baustoffe</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--ClientOrderNumber">1154034</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Supplier">KLK</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--ArticleNumber">30315704</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Name">STANDVENTILATOR COOLBR_4000_SV D40CM</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Quantity">2 Stk.</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--UnitPrice">951,92&euro;</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Price">1903,84 &euro;</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Status">erledigt</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Return"><Checkbox /></Table.HeaderCell>
                            </Table.Row>
                            <Table.Row className="OrderInfoList__Position">
                                <Table.HeaderCell colspan="6"></Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--ArticleNumber">30315704</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Name">STANDVENTILATOR COOLBR_4000_SV D40CM</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Quantity">2 Stk.</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--UnitPrice">951,92&euro;</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Price">1903,84 &euro;</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Status">erledigt</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Return"><Checkbox /></Table.HeaderCell>
                            </Table.Row>
                            <Table.Row className="OrderInfoList__Position">
                                <Table.HeaderCell colspan="6"></Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--ArticleNumber">30315704</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Name">STANDVENTILATOR COOLBR_4000_SV D40CM</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Quantity">2 Stk.</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--UnitPrice">951,92&euro;</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Price">1903,84 &euro;</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Status">erledigt</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Return"><Checkbox /></Table.HeaderCell>
                            </Table.Row>

                            <Table.Row className="OrderInfoList__Position OrderInfoList__Position--Order">
                                <Table.HeaderCell className="OrderInfoList__Cell--OrderNumber">9127362</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Date">18.12.2019</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Client">7600</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--BusinessArea">Baustoffe</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--ClientOrderNumber">1154034</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Supplier">KLK</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--ArticleNumber">30315704</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Name">STANDVENTILATOR COOLBR_4000_SV D40CM</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Quantity">2 Stk.</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--UnitPrice">951,92&euro;</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Price">1903,84 &euro;</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Status">erledigt</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Return"><Checkbox /></Table.HeaderCell>
                            </Table.Row>
                            <Table.Row className="OrderInfoList__Position">
                                <Table.HeaderCell colspan="6"></Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--ArticleNumber">30315704</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Name">STANDVENTILATOR COOLBR_4000_SV D40CM</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Quantity">2 Stk.</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--UnitPrice">951,92&euro;</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Price">1903,84 &euro;</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Status">erledigt</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Return"><Checkbox /></Table.HeaderCell>
                            </Table.Row>
                            <Table.Row className="OrderInfoList__Position">
                                <Table.HeaderCell colspan="6"></Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--ArticleNumber">30315704</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Name">STANDVENTILATOR COOLBR_4000_SV D40CM</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Quantity">2 Stk.</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--UnitPrice">951,92&euro;</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Price">1903,84 &euro;</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Status">erledigt</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Return"><Checkbox /></Table.HeaderCell>
                            </Table.Row>

                            <Table.Row className="OrderInfoList__Position OrderInfoList__Position--Order">
                                <Table.HeaderCell className="OrderInfoList__Cell--OrderNumber">9127362</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Date">18.12.2019</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Client">7600</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--BusinessArea">Baustoffe</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--ClientOrderNumber">1154034</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Supplier">KLK</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--ArticleNumber">30315704</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Name">STANDVENTILATOR COOLBR_4000_SV D40CM</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Quantity">2 Stk.</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--UnitPrice">951,92&euro;</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Price">1903,84 &euro;</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Status">erledigt</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Return"><Checkbox /></Table.HeaderCell>
                            </Table.Row>
                            <Table.Row className="OrderInfoList__Position">
                                <Table.HeaderCell colspan="6"></Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--ArticleNumber">30315704</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Name">STANDVENTILATOR COOLBR_4000_SV D40CM</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Quantity">2 Stk.</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--UnitPrice">951,92&euro;</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Price">1903,84 &euro;</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Status">erledigt</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Return"><Checkbox /></Table.HeaderCell>
                            </Table.Row>
                            <Table.Row className="OrderInfoList__Position">
                                <Table.HeaderCell colspan="6"></Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--ArticleNumber">30315704</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Name">STANDVENTILATOR COOLBR_4000_SV D40CM</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Quantity">2 Stk.</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--UnitPrice">951,92&euro;</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Price">1903,84 &euro;</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Status">erledigt</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Return"><Checkbox /></Table.HeaderCell>
                            </Table.Row>

                            <Table.Row className="OrderInfoList__Position OrderInfoList__Position--Order">
                                <Table.HeaderCell className="OrderInfoList__Cell--OrderNumber">9127362</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Date">18.12.2019</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Client">7600</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--BusinessArea">Baustoffe</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--ClientOrderNumber">1154034</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Supplier">KLK</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--ArticleNumber">30315704</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Name">STANDVENTILATOR COOLBR_4000_SV D40CM</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Quantity">2 Stk.</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--UnitPrice">951,92&euro;</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Price">1903,84 &euro;</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Status">erledigt</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Return"><Checkbox /></Table.HeaderCell>
                            </Table.Row>
                            <Table.Row className="OrderInfoList__Position">
                                <Table.HeaderCell colspan="6"></Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--ArticleNumber">30315704</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Name">STANDVENTILATOR COOLBR_4000_SV D40CM</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Quantity">2 Stk.</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--UnitPrice">951,92&euro;</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Price">1903,84 &euro;</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Status">erledigt</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Return"><Checkbox /></Table.HeaderCell>
                            </Table.Row>
                            <Table.Row className="OrderInfoList__Position">
                                <Table.HeaderCell colspan="6"></Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--ArticleNumber">30315704</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Name">STANDVENTILATOR COOLBR_4000_SV D40CM</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Quantity">2 Stk.</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--UnitPrice">951,92&euro;</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Price">1903,84 &euro;</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Status">erledigt</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Return"><Checkbox /></Table.HeaderCell>
                            </Table.Row>

                            <Table.Row className="OrderInfoList__Position OrderInfoList__Position--Order">
                                <Table.HeaderCell className="OrderInfoList__Cell--OrderNumber">9127362</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Date">18.12.2019</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Client">7600</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--BusinessArea">Baustoffe</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--ClientOrderNumber">1154034</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Supplier">KLK</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--ArticleNumber">30315704</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Name">STANDVENTILATOR COOLBR_4000_SV D40CM</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Quantity">2 Stk.</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--UnitPrice">951,92&euro;</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Price">1903,84 &euro;</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Status">erledigt</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Return"><Checkbox /></Table.HeaderCell>
                            </Table.Row>
                            <Table.Row className="OrderInfoList__Position">
                                <Table.HeaderCell colspan="6"></Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--ArticleNumber">30315704</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Name">STANDVENTILATOR COOLBR_4000_SV D40CM</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Quantity">2 Stk.</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--UnitPrice">951,92&euro;</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Price">1903,84 &euro;</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Status">erledigt</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Return"><Checkbox /></Table.HeaderCell>
                            </Table.Row>
                            <Table.Row className="OrderInfoList__Position">
                                <Table.HeaderCell colspan="6"></Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--ArticleNumber">30315704</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Name">STANDVENTILATOR COOLBR_4000_SV D40CM</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Quantity">2 Stk.</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--UnitPrice">951,92&euro;</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Price">1903,84 &euro;</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Status">erledigt</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Return"><Checkbox /></Table.HeaderCell>
                            </Table.Row>

                            <Table.Row className="OrderInfoList__Position OrderInfoList__Position--Order">
                                <Table.HeaderCell className="OrderInfoList__Cell--OrderNumber">9127362</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Date">18.12.2019</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Client">7600</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--BusinessArea">Baustoffe</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--ClientOrderNumber">1154034</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Supplier">KLK</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--ArticleNumber">30315704</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Name">STANDVENTILATOR COOLBR_4000_SV D40CM</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Quantity">2 Stk.</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--UnitPrice">951,92&euro;</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Price">1903,84 &euro;</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Status">erledigt</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Return"><Checkbox /></Table.HeaderCell>
                            </Table.Row>
                            <Table.Row className="OrderInfoList__Position">
                                <Table.HeaderCell colspan="6"></Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--ArticleNumber">30315704</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Name">STANDVENTILATOR COOLBR_4000_SV D40CM</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Quantity">2 Stk.</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--UnitPrice">951,92&euro;</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Price">1903,84 &euro;</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Status">erledigt</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Return"><Checkbox /></Table.HeaderCell>
                            </Table.Row>
                            <Table.Row className="OrderInfoList__Position">
                                <Table.HeaderCell colspan="6"></Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--ArticleNumber">30315704</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Name">STANDVENTILATOR COOLBR_4000_SV D40CM</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Quantity">2 Stk.</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--UnitPrice">951,92&euro;</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Price">1903,84 &euro;</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Status">erledigt</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Return"><Checkbox /></Table.HeaderCell>
                            </Table.Row>

                            <Table.Row className="OrderInfoList__Position OrderInfoList__Position--Order">
                                <Table.HeaderCell className="OrderInfoList__Cell--OrderNumber">9127362</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Date">18.12.2019</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Client">7600</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--BusinessArea">Baustoffe</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--ClientOrderNumber">1154034</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Supplier">KLK</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--ArticleNumber">30315704</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Name">STANDVENTILATOR COOLBR_4000_SV D40CM</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Quantity">2 Stk.</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--UnitPrice">951,92&euro;</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Price">1903,84 &euro;</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Status">erledigt</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Return"><Checkbox /></Table.HeaderCell>
                            </Table.Row>
                            <Table.Row className="OrderInfoList__Position">
                                <Table.HeaderCell colspan="6"></Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--ArticleNumber">30315704</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Name">STANDVENTILATOR COOLBR_4000_SV D40CM</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Quantity">2 Stk.</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--UnitPrice">951,92&euro;</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Price">1903,84 &euro;</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Status">erledigt</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Return"><Checkbox /></Table.HeaderCell>
                            </Table.Row>
                            <Table.Row className="OrderInfoList__Position">
                                <Table.HeaderCell colspan="6"></Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--ArticleNumber">30315704</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Name">STANDVENTILATOR COOLBR_4000_SV D40CM</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Quantity">2 Stk.</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--UnitPrice">951,92&euro;</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Price">1903,84 &euro;</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Status">erledigt</Table.HeaderCell>
                                <Table.HeaderCell className="OrderInfoList__Cell--Return"><Checkbox /></Table.HeaderCell>
                            </Table.Row>

                        </Table.Body>
                    </Table>
                </div>
            }
        </div>

    );
};

export default OrderInfoList;