
import React, { useState } from "react";
import { Input, Table, Grid, Header, Button, Accordion, GridRow, GridColumn, Segment, TableRow, TableBody } from "semantic-ui-react";
import "./OrderInfoListQuickLinks.css"

// Not used
const OrderInfoListQuickLinks = (props) => {

    const [isFiltered, setIsFiltered] = useState(false);

    return (
        <div className="OrderInfoList__QuickLinks">
            <Grid columns="13" padded="vertically">
                <GridRow stretched>
                    <GridColumn width="3">
                        <Segment basic className="OrderInfoList__QuickLinks__Guide">
                            <Header as="h3">So funktioniert's</Header>
                            <p>Mit den gewünschten Eckdaten finden Sie rasch Ihren Auftrag um:</p>
                            <p>- den <b>Status</b> abzufragen<br />
                                - eine <b>Retoure</b> anzusto&szlig;en<br />
                                - etc.</p>
                        </Segment>
                    </GridColumn>
                    <GridColumn width="13">
                        <Segment basic className="OrderInfoList__QuickLinks__Options">
                            <Header as="h3">Häufig gesucht</Header>
                            <Grid columns="4">
                                <GridRow>
                                    <GridColumn>
                                        <Button fluid onClick={() => setIsFiltered(true)}>Heute</Button>
                                    </GridColumn>
                                    <GridColumn>
                                        <Button fluid onClick={() => setIsFiltered(true)}>Diese Woche</Button>
                                    </GridColumn>
                                    <GridColumn>
                                        <Button fluid onClick={() => setIsFiltered(true)}>Diesen Monat</Button>
                                    </GridColumn>
                                    <GridColumn>
                                        <Button fluid onClick={() => setIsFiltered(true)}>offene Aufträge</Button>
                                    </GridColumn>
                                </GridRow>
                                <GridRow>
                                    <GridColumn>
                                        <Button fluid onClick={() => setIsFiltered(true)}>... etc.</Button>
                                    </GridColumn>
                                    <GridColumn>
                                        <Button fluid onClick={() => setIsFiltered(true)}>Die letzten 30</Button>
                                    </GridColumn>
                                </GridRow>
                            </Grid>
                        </Segment>
                    </GridColumn>
                </GridRow>
            </Grid>
        </div>
    );
};

export default OrderInfoListAccordion;