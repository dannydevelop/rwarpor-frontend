import React from "react";
import {
    Grid,
    GridColumn,
    GridRow,
} from "semantic-ui-react";
// import OrderInfoList from "./OrderInfoList/OrderInfoList";
import OrderInfoFilter from "./OrderInfoFilter/OrderInfoFilter";
import OrderInfoList from "./OrderInfoList/OrderInfoList";

export default () => (
    <>
        <Grid>
            <GridRow>
                <GridColumn width="3">
                    <OrderInfoFilter/>
                </GridColumn>
                <GridColumn width="13">
                    <OrderInfoList/>
                </GridColumn>
            </GridRow>
        </Grid>
    </>
);