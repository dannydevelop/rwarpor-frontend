import React from "react";
import "./OrderInfoFilter.css";
import { Form, Button, Accordion } from "semantic-ui-react";
import SemanticDatepicker from "react-semantic-ui-datepickers";

const supplierOptions = [
    {
        key: 'some',
        text: 'BRT',
        value: 'brt'
    }, {
        key: 'other',
        text: 'LAX',
        value: 'lax'
    }
];

const panels = [
    {
        key: 'details',
        title: 'Suche verfeinern',
        content: {
            as: Form.Input,
            label: 'Belegnummer',
            placeholder: '128382723',
        },
    },
]
const OrderInfoFilter = () => {

    return (
        <Form>
            <Form.Dropdown
                label="Verkaufsorganisation"
                placeholder="keine Auswahl"
                search selection options={supplierOptions}
            />

            <div className="field OrderInfoFilter__Field OrderInfoFilter__Field--Date">
                <label>Von</label>
                <SemanticDatepicker fluid locale="de-DE" />
            </div>

            <div className="field OrderInfoFilter__Field OrderInfoFilter__Field--Date">
                <label>Bis</label>
                <SemanticDatepicker locale="de-DE" />
            </div>

            <Accordion as={Form.Field} panels={panels} />

            <Button primary fluid type="submit">Auftrag suchen</Button>

        </Form>
    );
};

export default OrderInfoFilter;