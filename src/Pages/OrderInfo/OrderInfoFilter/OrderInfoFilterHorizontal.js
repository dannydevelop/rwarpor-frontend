
import React from "react";
import "./OrderInfoFilter.css";
import { Form, Button, Accordion, FormGroup } from "semantic-ui-react";
import SemanticDatepicker from "react-semantic-ui-datepickers";

const supplierOptions = [
    {
        key: 'some',
        text: 'BRT',
        value: 'brt'
    }, {
        key: 'other',
        text: 'LAX',
        value: 'lax'
    }
];

const panels = [
    {
        key: 'details',
        title: 'Suche verfeinern',
        content: {
            as: Form.Input,
            label: 'Belegnummer',
            placeholder: '128382723',
        },
    },
]
const OrderInfoFilter = () => {

    return (
        <Form className="OrderInfoFilterHorizontal">
            <FormGroup  widths='equal'>
                <Form.Dropdown
                    label="Verkaufsorganisation"
                    placeholder="keine Auswahl"
                    search selection options={supplierOptions}
                />

                <div className="field OrderInfoFilter__Field OrderInfoFilter__Field--Date">
                    <label>Von</label>
                    <SemanticDatepicker fluid locale="de-DE" />
                </div>

                <div className="field OrderInfoFilter__Field OrderInfoFilter__Field--Date">
                    <label>Bis</label>
                    <SemanticDatepicker locale="de-DE" />
                </div>
                <Button primary fluid type="submit">Auftrag suchen</Button>

            </FormGroup>

            <Accordion as={Form.Field} panels={panels} />

        </Form>
    );
};

export default OrderInfoFilter;