import React from "react";
import {
  Message,
  MessageHeader,
} from "semantic-ui-react";
import Favorites from "./Favorites/Favorites";
import ServiceSearch from "./ServiceSearch/ServiceSearch";

const Dashboard = () => {

  const handleDismiss = () => {
    console.log('Message dismissed!')
  }

  return (
    <>
      <Message info onDismiss={handleDismiss}>
        <MessageHeader>12.01.2020 - Styleguide geht online bald</MessageHeader>
        <p>
          Auf Grund von Wartungsarbeiten steht der RWA-Shop von 14. Dezember 20:00 Uhr bis vorraussichtlich 15. Dezember 20:00 Uhr nicht zur Verfügung. Wir bitten um Ihr Verständnis.
      </p>
      </Message>
      <Favorites />
      <ServiceSearch />
    </>
  );
}
export default Dashboard;