import React, { Component } from "react";
import { Icon, Header, Card } from "semantic-ui-react";
import { Link } from "react-router-dom";

import "./Favorites.css";

export default class Favorites extends Component {

  render() {
    return (
      <>
        <Header as="h2">TOP SERVICES</Header>

        <Card.Group itemsPerRow={8}>
          <Card color="green" className="Favorite">
            <Card.Content className="Favorite__Content">
              <Link to="/order-info">
                <Icon className="Favorite__Icon" name="clipboard outline" />
                <Card.Header className="Favorite__Header">
                  Auftrags-<br />Auskunft
              </Card.Header>
              </Link>
            </Card.Content>
          </Card>
          <Card color="green" className="Favorite">
            <Card.Content className="Favorite__Content">
              <Link to="/order-info-version">
                <Icon className="Favorite__Icon" name="clipboard outline" />
                <Card.Header className="Favorite__Header">
                  *Auftrags-<br />Auskunft
              </Card.Header>
              </Link>
            </Card.Content>
          </Card>
          <Card color="green" className="Favorite disabled">
            <Card.Content className="Favorite__Content">
              <Icon className="Favorite__Icon" name="cart" />
              <Card.Header className="Favorite__Header">RWA-Shop</Card.Header>

            </Card.Content>
          </Card>
          <Card color="green" className="Favorite disabled">
            <Card.Content className="Favorite__Content">
              <Icon className="Favorite__Icon" name="building outline" />
              <Card.Header className="Favorite__Header">Ordermesse</Card.Header>

            </Card.Content>
          </Card>
          <Card color="green" className="Favorite disabled">
            <Card.Content className="Favorite__Content">
              <Icon className="Favorite__Icon" name="percent" />
              <Card.Header className="Favorite__Header">
                Aktions-Bestellung
              </Card.Header>

            </Card.Content>
          </Card>
          <Card color="green" className="Favorite disabled">
            <Card.Content className="Favorite__Content">
              <Icon className="Favorite__Icon" name="print" />
              <Card.Header className="Favorite__Header">
                Rechnungs-Auskunft
              </Card.Header>

            </Card.Content>
          </Card>
        </Card.Group>
      </>
    );
  }
}
