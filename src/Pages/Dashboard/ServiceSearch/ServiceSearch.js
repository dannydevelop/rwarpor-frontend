import React, { Component } from "react";
import {
  Input,
  Menu,
  Segment,
  Icon,
  Grid,
  Header,
} from "semantic-ui-react";
import { Link } from "react-router-dom";

import "./ServiceSearch.css";

export default class ServiceSearch extends Component {
  state = { activeTab: "sales-related", activeItem: "none" };

  handleTabClick = (e, { name }) => this.setState({ activeTab: name });

  handleItemClick = (e, { name }) => this.setState({ activeItem: name });

  handleRemoveFavorite = (e, { name }) => console.log("remove Favorite");

  render() {
    const { activeTab, activeItem } = this.state;

    return (
      <div className="ServiceSearch">
        <Header as="h2">ALLE SERVICES IM ÜBERBLICK</Header>
        <Menu pointing secondary className="ServiceSearch__Filter">
          <Menu.Item
            className="ServiceSearch__Menu__Item"
            name="sales-related"
            active={activeTab === "sales-related"}
            onClick={this.handleTabClick}
          >
            Rund um den Einkauf
          </Menu.Item>
          <Menu.Item
            className="ServiceSearch__Menu__Item"
            name="internal"
            active={activeTab === "internal"}
            onClick={this.handleTabClick}
          >
            RWA intern
          </Menu.Item>
          <Menu.Item
            className="ServiceSearch__Menu__Item"
            name="info-services"
            active={activeTab === "info-services"}
            onClick={this.handleTabClick}
          >
            Info-Services
          </Menu.Item>
          <Menu.Menu position="right">
            <Menu.Item className="ServiceSearch__Menu__Item ServiceSearch__Menu__Item--Searchbox">
              <Input icon="search" placeholder="Suchen ..." />
            </Menu.Item>
          </Menu.Menu>
        </Menu>
        <Segment basic className="Results">
          <Grid columns={4}>
            <Grid.Row>
              <Grid.Column>
                <Header as="h4">Einkauf</Header>
                <Menu fluid vertical>
                  <Menu.Item
                    className="Results__Menu__Item"
                    name="rwa-shop"
                    active={activeItem === "rwa-shop"}
                    onClick={this.handleItemClick}
                  >
                    <Icon className="Results__Menu__Item__Icon" name="cart" />
                    RWA-Shop
                    
                  </Menu.Item>
                  <Menu.Item
                    className="Results__Menu__Item"
                    name="order-fair"
                    active={activeItem === "order-fair"}
                    onClick={this.handleItemClick}
                  >
                    <Icon
                      className="Results__Menu__Item__Icon"
                      name="building"
                    />
                    Order-Messe
                    
                  </Menu.Item>
                  <Menu.Item
                    className="Results__Menu__Item"
                    name="campaign-order"
                    active={activeItem === "campaign-order"}
                    onClick={this.handleItemClick}
                  >
                    <Icon
                      className="Results__Menu__Item__Icon"
                      name="percent"
                    />
                    Aktionsbestellung
                    
                  </Menu.Item>
                </Menu>
              </Grid.Column>
              <Grid.Column>
                <Header as="h4">Lagerwirtschaft</Header>
                <Menu vertical fluid>
                  <Menu.Item
                    className="Results__Menu__Item"
                    name="order-info"
                    active={activeItem === "order-info"}
                    onClick={this.handleItemClick}
                  >
                    <Link to="/order-info">
                      <Icon
                        className="Results__Menu__Item__Icon"
                        name="clipboard"
                      />
                      Auftrags-Auskunft
                    </Link>
                    
                  </Menu.Item>
                  <Menu.Item
                    className="Results__Menu__Item"
                    name="delivery-note-info"
                    active={activeItem === "delivery-note-info"}
                    onClick={this.handleItemClick}
                  >
                    <Icon
                      className="Results__Menu__Item__Icon"
                      name="gamepad"
                    />
                    Lieferschein-Auskunft
                    
                  </Menu.Item>
                  <Menu.Item
                    className="Results__Menu__Item"
                    name="invoice-info"
                    active={activeItem === "invoice-info"}
                    onClick={this.handleItemClick}
                  >
                    <Icon className="Results__Menu__Item__Icon" name="print" />
                    Rechnungs-Auskunft
                    
                  </Menu.Item>
                </Menu>
              </Grid.Column>
              <Grid.Column>
                <Header as="h4">Weitere</Header>
                <Menu vertical fluid>
                  <Menu.Item
                    className="Results__Menu__Item"
                    name="retour"
                    active={activeItem === "retour"}
                    onClick={this.handleItemClick}
                  >
                    <Icon
                      className="Results__Menu__Item__Icon"
                      name="gamepad"
                    />
                    Retour
                    
                  </Menu.Item>
                </Menu>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Segment>
      </div>
    );
  }
}
