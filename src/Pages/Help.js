import React from "react";
import {
  Header,
  Segment
} from "semantic-ui-react";

export default () => (
  <Segment basic>
    <Header as="h1">Help</Header>
    <p>Informativer Text</p>
  </Segment>
);

