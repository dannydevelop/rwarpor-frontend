import React from "react";
import {
  Button,
  Form,
  Grid,
  Header,
  Icon,
  Modal,
  Segment
} from "semantic-ui-react";

const ModalPasswortReset = () => {
  // handler recieves the `e` event object
  const preventDefault = (e) => { 
    e.preventDefault();
  }

  return (
    <Modal trigger={<Button onClick={preventDefault}>Zurücksetzen</Button>}>
      <Modal.Header>Passwort Reset</Modal.Header>
      <Modal.Content>
        <Modal.Description>
          <p>
            Pellentesque habitant morbi tristique senectus et netus et malesuada
            fames ac turpis egestas. Vestibulum.
        </p>
          <Form>
            <Form.Input label="Email" placeholder="joe@schmoe.com" />
            <Button>Zurücksetzen</Button>
          </Form>
        </Modal.Description>
      </Modal.Content>
    </Modal>
  );
}

const LoginForm = () => (
  <Grid textAlign="center" style={{ height: "100%" }} verticalAlign="middle">
    <Grid.Column style={{ maxWidth: 450 }}>
      <Form size="large" action="/dashboard">
        <Segment basic>
          <Header as="h2" textAlign="center" icon>
            <Icon name="users" />
            <Header.Content>Willkommen</Header.Content>
          </Header>
          <Form.Input
            fluid
            icon="user"
            iconPosition="left"
            placeholder="E-mail Adresse"
          />
          <Form.Input
            fluid
            icon="lock"
            iconPosition="left"
            placeholder="Passwort"
            type="password"
          />

          <Button primary fluid size="large">
            Login
          </Button>
          <p>
            Passwort vergessen? <ModalPasswortReset />
          </p>
        </Segment>
      </Form>
    </Grid.Column>
  </Grid>
);

export default LoginForm;
