import React from "react";
import LoginForm from "../../Pages/Login";
import Help from '../../Pages/Help';
import Impressum from "../../Pages/Impressum";
import Dashboard from "../../Pages/Dashboard/Dashboard";
import Profile from "../../Pages/Profile";
import OrderInfo from "../../Pages/OrderInfo/OrderInfo";
import OrderInfoVersion from "../../Pages/OrderInfo/OrderInfoVersion";
import Contact from "../../Pages/Contact";

import { Container } from "semantic-ui-react";
import { Route } from "react-router-dom";
import "./Main.css";

export default () => {
    return (
      <Container className="Main">
        <Route path="/" exact component={LoginForm} />
        <Route path="/help" component={Help} />
        <Route path="/profile" component={Profile} />
        <Route path="/impressum" component={Impressum} />
        <Route path="/dashboard" component={Dashboard} />
        <Route path="/contact" component={Contact} />
        <Route path="/order-info" component={OrderInfo} />
        <Route path="/order-info-version" component={OrderInfoVersion} />
      </Container>
    );
}