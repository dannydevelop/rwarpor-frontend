import React, { Component } from "react";
import { Menu, Container } from "semantic-ui-react";
import { Link } from 'react-router-dom';
import "./Footer.css";


export default class Footer extends Component {
  render() {
    return (
      <footer className="Footer">
        <Container>
            <Menu text borderless>
              <Menu.Item as={ Link } to="/help">
                  Hilfe
              </Menu.Item>
              <Menu.Item as={ Link } to="/contact">
                  Kontakt
              </Menu.Item>
              <Menu.Item as={ Link } to="/impressum">
                  AGB's &amp; Impressum
              </Menu.Item>
            </Menu>
        </Container>
      </footer>
    );
  }
}
