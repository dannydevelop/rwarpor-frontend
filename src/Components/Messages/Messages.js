import React, { Component } from "react";
import { Segment, Button, Icon, Message } from "semantic-ui-react";
import "./news.css";

export default class Messages extends Component {

  render() {
    return (
      <Segment basic className="Messages">
        <Message>
          <Message.Header>
            <Icon name="building"/> Service Ausfallzeiten
          </Message.Header>
          <Message.Content>
            Von Dienstag bis Donnerstag ist die Order Messe abgeschalten. Mehrere Termine entnehmen Sie dem PDF.
          <Button size="mini" fluid>
              <Icon name="download" />
              Wartungsplan.pdf
              </Button>
          </Message.Content>
        </Message>
      </Segment>
    );
  }
}
