import React, { Component } from "react";
import { Menu, Container, Icon, Header } from "semantic-ui-react";
import { Link } from "react-router-dom";
import "./FixedHeader.css";

export default class FixedHeader extends Component {

  render() {

    return (
      <Menu className="Navigation" fixed="top">
        <Container>
          <Menu.Item
            className="Navigation__Logo"
            name="home"
          >
            <Link to="dashboard">
              <svg id="icon-rwa" viewBox="0 0 112 32">
                <path
                  fill="#000"
                  d="M101.839 9.319l2.639 9.6h-5.277l2.639-9.6zM105.263 4.772h-6.905l-6.961 22.905h5.389l1.235-4.491h7.691l1.235 4.491h5.389l-7.074-22.905zM91.958 4.772h-5.109l-3.874 16.393-3.93-16.393h-6.288l-3.986 16.393-3.874-16.393h-5.109l5.614 22.905h6.568l3.874-15.551 3.874 15.551h6.568l5.67-22.905zM52.211 13.586c0 1.74-0.561 2.526-2.751 2.526h-4.098v-6.737h4.098c2.077 0 2.751 0.674 2.751 2.414v1.796zM53.446 27.677h6.119l-5.558-7.86c2.302-0.954 3.312-2.863 3.312-5.726v-2.807c0-4.323-2.077-6.512-7.635-6.512h-9.544v22.905h5.165v-7.13h3.032l5.109 7.13z"
                />
                <path
                  fill="#009640"
                  d="M0 31.888h31.888v-31.888h-31.888v31.888zM20.94 14.147l-1.684 1.684 8.533 8.533-3.368 3.368-8.477-8.589-8.477 8.477-3.368-3.368 8.477-8.477-1.684-1.684v-3.312l-1.179-1.179v2.751l-1.179 1.179-4.154-4.154-1.123 1.123c0-0.73 0.168-3.2 2.133-5.109 2.246-2.189 4.435-0.898 5.221-0.112l4.042 4.042-1.011 1.011 2.246 2.189 2.246-2.189-1.011-1.011 4.042-4.042c0.786-0.842 2.975-2.133 5.221 0.056 1.965 1.965 2.189 4.435 2.189 5.165l-1.123-1.123-4.211 4.211-1.123-1.235v-2.751l-1.179 1.179v3.368z"
                />
              </svg>
            </Link>
          </Menu.Item>
          <Menu.Item className="Navigation__Back">
            <Link to="dashboard">
              <Icon name="caret left"/>
              Zurück
            </Link>
          </Menu.Item>
          <Menu.Item className="Navigation__CurrentService">
            <Header as="h3">Auftragsauskunft</Header>
          </Menu.Item>
          <div className="divider" />
          <Menu.Item className="Navigation__Profile">
            <Icon name="user" />
            Friedrich W. Raiffeisen
          </Menu.Item>
          <Menu.Item
            className="Navigation__Menu__Item"
            name="profile"
          >
            <Link to="/profile">
              Profil
            </Link>
          </Menu.Item>
          <Menu.Item
            className="Navigation__Menu__Item"
            name="logout"
          >
            <Link to="/">
            Logout
            </Link>
          </Menu.Item>
        </Container>
      </Menu>
    );
  }
}
