import React from 'react';
import ResponsiveContainer from './Components/ResponsiveContainer/ResponsiveContainer'
import Main from './Components/Main/Main';
import FixedHeader from './Components/FixedHeder/FixedHeader';
import Footer from './Components/Footer/Footer';
import './App.css';


export default () => {
  return (
    <ResponsiveContainer>
      <FixedHeader />
      <Main />
      <Footer />
    </ResponsiveContainer>
  );
}
